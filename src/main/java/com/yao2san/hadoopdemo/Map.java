package com.yao2san.hadoopdemo;

import org.apache.commons.lang.text.StrTokenizer;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class Map extends Mapper<LongWritable, Text, Text, IntWritable> {
    private final static IntWritable ONE = new IntWritable(1);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        StrTokenizer strTokenizer = new StrTokenizer(line);
        while (strTokenizer.hasNext()) {
            context.write(new Text(strTokenizer.nextToken()), ONE);
        }
    }
}
