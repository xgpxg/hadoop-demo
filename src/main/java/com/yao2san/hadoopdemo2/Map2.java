package com.yao2san.hadoopdemo2;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

public class Map2 extends Mapper<LongWritable, Text, IntWritable, Text> {
    private final static IntWritable ONE = new IntWritable(1);

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        String[] spilt = line.split(" ");
        if (spilt.length == 2) {
            context.write(new IntWritable(Integer.valueOf(spilt[1])), new Text(spilt[0]));
        }
    }
}
